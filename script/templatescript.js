$(document).ready(function(){
  $('.collapsible').collapsible();
});

function i(IDdoObjeto) {
	return document.getElementById(IDdoObjeto);
}
var docCookies = {
  getItem: function (sKey) {
    if (!sKey) { return null; }
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
  },
  setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
        sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
        break;
        case String:
        sExpires = "; expires=" + vEnd;
        break;
        case Date:
        sExpires = "; expires=" + vEnd.toUTCString();
        break;
      }
    }
    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    return true;
  },
  removeItem: function (sKey, sPath, sDomain) {
    if (!this.hasItem(sKey)) { return false; }
    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
    return true;
  },
  hasItem: function (sKey) {
    if (!sKey) { return false; }
    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
  },
  keys: function () {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
      return aKeys;
  }
};



window.onload = function(){

	var cordeFundo = docCookies.getItem('cordeFundo') || "vermelho";
	var body = document.getElementsByTagName('body')[0];
	body.className=cordeFundo;

	i('corVermelho').onclick = function(){
		body.className = 'vermelho';
		cordeFundo = 'vermelho';
		docCookies.setItem('cordeFundo', cordeFundo);
	};
	i('corAmarelo').onclick = function(){
		body.className = 'amarelo';
		cordeFundo = 'amarelo';
		docCookies.setItem('cordeFundo', cordeFundo);
	};
	
	i('corLaranja').onclick = function(){
		body.className = 'laranja';
		cordeFundo = 'laranja';
		docCookies.setItem('cordeFundo', cordeFundo);
	};

	
	
	var tamanho =  docCookies.getItem('tamanho') || 100;  // getCookie('tamanho');

	document.getElementById('meubody').style.fontSize = tamanho + '%';
	
	document.getElementById('fonteMenos').onclick = function() {
		tamanho --;
		if(tamanho < 50) tamanho = 50;
		document.getElementById('meubody').style.fontSize = tamanho + '%';
		docCookies.setItem('tamanho', tamanho);

	};
	document.getElementById('fonteNormal').onclick = function() {
		tamanho = 100;
		document.getElementById('meubody').style.fontSize = tamanho + '%';
		docCookies.setItem('tamanho', tamanho);
	};
	document.getElementById('fonteMais').onclick = function() {
		tamanho ++;
		if(tamanho > 200) tamanho = 200;
		document.getElementById('meubody').style.fontSize = tamanho + '%';
		docCookies.setItem('tamanho', tamanho);
	};

};


// JAVASCRIP COOKIE EXAMPLE - W3SCHOOLS
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie() {
  var user = getCookie("username");
  if (user != "") {
    alert("Welcome again " + user);
  } else {
    user = prompt("Please enter your name:", "");
    if (user != "" && user != null) {
      setCookie("username", user, 365);
    }
  }
}

